#include "config.h"

#define PIN_RED 2 // NODEMCU PORT 4 = Arduinopin 2
#define PIN_GREEN 4 // NODEMCU PORT 2 = Arduinopin 4
#define PIN_BLUE 5 // NODEMCU PORT 1 = Arduinopin 5

#include "ESP8266WiFi.h"
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"

#define IO_SERVER "io.adafruit.com"
#define IO_SERVERPORT 1883

WiFiManager wifiManager;
WiFiClient client;

Adafruit_MQTT_Client mqtt(&client, IO_SERVER, IO_SERVERPORT, IO_USERNAME, IO_USERNAME, IO_KEY);
Adafruit_MQTT_Subscribe ifttmessage = Adafruit_MQTT_Subscribe(&mqtt, IO_USERNAME "/feeds/incoming-ifttt", MQTT_QOS_1);

void setup() {
  pinMode(PIN_RED, OUTPUT);
  pinMode(PIN_GREEN, OUTPUT);
  pinMode(PIN_BLUE, OUTPUT);

  // start the serial connection
  Serial.begin(115200);
  delay(10);

  Serial.println(F("iot-lamp"));

  WiFiManager wifiManager;
  wifiManager.autoConnect();
  
  Serial.println("WiFi connected!!");
  Serial.println("IP address: "); Serial.println(WiFi.localIP());
  
  ifttmessage.setCallback(handleMessage);
  mqtt.subscribe(&ifttmessage);
}

void loop() {
  MQTT_connect();
  mqtt.processPackets(10000);
 
  if(! mqtt.ping()) {
    mqtt.disconnect();
  }
}

void handleMessage(char *text, uint16_t len) {
  bool hasReceived = false;
  String receivedText = String(text);
  
  Serial.print("received: ");
  Serial.println(receivedText);

  if (receivedText == "email") {
    hasReceived = true;
    Serial.println("emailprocessed");
    setColor(255, 0, 0);
  } else if (receivedText == "facebook") {
    hasReceived = true;
    setColor(0, 255, 0);
  } else if (receivedText == "twitter") {
    hasReceived = true;
    setColor(0, 0, 255);
  } else if (receivedText == "calendar-start") {
    
  } else if (receivedText == "calendar-near-end") {
    
  } else if (receivedText == "calendar-end") {
    
  } else if (receivedText == "blue-permanent") {
    setColor(0, 0, 255);
  } else if (receivedText == "red-permanent") {
    setColor(255, 0, 0);
  } else if (receivedText == "green-permanent") {
    setColor(0, 255, 0);
  } else if (receivedText == "white-permanent") {
    setColor(255, 255, 255);
  }

  if (hasReceived) {
    delay(5000);
    setColor(0, 0, 0);
  }
}

void setColor(uint16_t redValue, uint16_t greenValue, uint16_t blueValue) {
  analogWrite(PIN_RED, redValue);
  analogWrite(PIN_GREEN, greenValue);
  analogWrite(PIN_BLUE, blueValue);
}

void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 10 seconds...");
       mqtt.disconnect();
       delay(10000);  // wait 10 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}

