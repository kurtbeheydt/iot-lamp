// INSTRUCTIONS:

// 1.
// Fill out your IO_USERNAME and IO_KEY

// 2.
// rename this file to config.h

// visit io.adafruit.com if you need to create an account,
// or if you need your Adafruit IO key.
#define IO_USERNAME  ""
#define IO_KEY       ""
